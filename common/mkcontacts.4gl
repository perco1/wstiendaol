IMPORT FGL libutil

FUNCTION create_database(tp,ws)
    DEFINE tp VARCHAR(20), ws BOOLEAN
    DEFINE tmp VARCHAR(200), s INTEGER

    WHENEVER ERROR CONTINUE
    DROP TABLE datafilter
    --
    DROP TABLE contnote
    DROP TABLE contact
    DROP TABLE city
    WHENEVER ERROR STOP

    IF tp=="cleanup" THEN
       RETURN
    END IF

    IF tp=="server" THEN

       LET s = libutil.sequence_drop("contact")
       --LET s = libutil.sequence_drop("contnote")

       CALL libutil.users_server_table()
       IF ws THEN
          CALL libutil.users_add(v_undef, NULL, v_undef_text) -- Foreign key to contacts
          CALL libutil.users_add("crystina",   NULL, "Crystina Sepulveda")
          CALL libutil.users_add("ingrid",  NULL, "Ingrid Canel")
          CALL libutil.users_add("paola",   NULL, "Paola Zelada")
       END IF

       CALL libutil.datafilter_table()
       IF ws THEN
          -- Data filters for user "crystina"
          LET tmp = " city_num = 1000 OR city_country IN ('REGION 9','REGION 10')"
          CALL libutil.datafilter_define("crystina", "city", tmp)
          CALL libutil.datafilter_define("crystina", "contact", "contact_city IN (select city_num FROM city WHERE"||tmp||")")
          -- Data filters for user "ingrid"
          LET tmp = " city_num = 1000 OR city_country IN ('France','Spain')"
          CALL libutil.datafilter_define("ingrid", "city", tmp)
          CALL libutil.datafilter_define("ingrid", "contact", "contact_city IN (select city_num FROM city WHERE"||tmp||")")
          -- Data filters for user "paola"
          CALL libutil.datafilter_define("paola", "city", NULL)
          CALL libutil.datafilter_define("paola", "contact", NULL)
       END IF

    END IF

    -- Readonly table for the mobile, filled with city of the user area (some countries)
    -- Filtered in the mobile application program, according to datafilter.
    CREATE TABLE city (
         city_num INTEGER NOT NULL,
         city_name VARCHAR(30) NOT NULL,
         city_country VARCHAR(30) NOT NULL,
         PRIMARY KEY(city_num),
         UNIQUE (city_name, city_country)
    )
    INSERT INTO city VALUES ( 1000, v_undef, v_undef_text )
    INSERT INTO city VALUES ( 1001, "REGION 9", "PETEN" )
    INSERT INTO city VALUES ( 1002, "REGION 10", "ALTA VERAPAZ" )
    INSERT INTO city VALUES ( 1003, "REGION 11", "BAJA VERAPAZ" )
    INSERT INTO city VALUES ( 1004, "REGION 30", "SAN LUIS" )
    INSERT INTO city VALUES ( 1005, "REGION 42", "PETEN SUR" )
    INSERT INTO city VALUES ( 1006, "REGION 91", "SANTA ELENA" )
    INSERT INTO city VALUES ( 1007, "REGION 92", "LA LIBERTAD" )
    INSERT INTO city VALUES ( 1008, "REGION 93", "QUICHE" )
    INSERT INTO city VALUES ( 1009, "REGION 94", "JOYABAJ" )
    INSERT INTO city VALUES ( 1010, "REGION 95", "HUEHUETENANGO" )
    INSERT INTO city VALUES ( 1011, "REGION 96", "LA DEMOCRACIA" )
    INSERT INTO city VALUES ( 1012, "REGION 97", "IZABAL" )

    CREATE TABLE contact (
         contact_num INTEGER NOT NULL,
         contact_rec_muser VARCHAR(20) NOT NULL,
         contact_rec_mtime DATETIME YEAR TO FRACTION(3) NOT NULL,
         contact_rec_mstat CHAR(2) NOT NULL,
         contact_name VARCHAR(100) NOT NULL,
         contact_operacion CHAR(12) NOT NULL,
         contact_valid CHAR(1) NOT NULL,
         contact_street VARCHAR(100),
         contact_city INTEGER NOT NULL,
         contact_num_m VARCHAR(40),
         contact_num_w VARCHAR(40),
         contact_num_h VARCHAR(40),
         contact_user VARCHAR(20) NOT NULL,
         contact_loc_lon DECIMAL(10,6),
         contact_loc_lat DECIMAL(10,6),
         contact_photo_mtime DATETIME YEAR TO FRACTION(3),
         contact_photo BYTE,
         PRIMARY KEY(contact_num),
         UNIQUE (contact_name, contact_city), -- contact_street) for unique tests
         FOREIGN KEY (contact_city) REFERENCES city (city_num),
         FOREIGN KEY (contact_user) REFERENCES users (user_id)
    )
    IF NOT ws THEN
       LET s = libutil.sequence_create("contact",1000)
    ELSE
       INSERT INTO contact VALUES ( 1001, "admin", "2010-01-01 00:00:00.000", "S",
              "ROBER OSWALDO MORALES HERNANDEZ HERNANDEZ", "7125123740", "Y", 
              "1 1 1 1 ALDEA CAROLINA -   CHISEC ALTA VERAPAZ",       1007, "03-7645-2345",
              NULL, NULL, "crystina", NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1002, "admin", "2010-01-01 00:00:00.000", "S",
              "RAFAEL ANTONIO DELGADO PAAU PAAU", "7049188409", "Y", 
              "7A. AVENIDA 11-17 ZONA 2 ZONA 2 SAN PEDRO CARCHA ALTA VERAPAZ",  1008, "03-1111-2345",
              NULL, NULL, v_undef, NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1003, "admin", "2010-01-01 00:00:00.000", "S",
              "DENIS OSWALDO MAQUIN CHIQUIN CHIQUIN", "7040222833", "Y", 
              "ALDEA LAS FLORES - IXCAN QUICHE",          1009, "03-9999-1111",
              NULL, NULL, "ingrid", NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1004, "admin", "2010-01-01 00:00:00.000", "S",
              "HUGO WALDEMAR SIQUIC TZUL TZUL", "7040240710",   "Y", 
              "LOTE 91     COLONIA LA REFORMA -   IXCAN QUICHE",        1010, "03-9999-2345",
              NULL, NULL, "paola", NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1005, "admin", "2010-01-01 00:00:00.000", "S",
              "ANTONIO MORALES SIANA SIANA", "7535008788", "Y", 
              "SAYAXCHE, CASERIO AGUA BLANCA, 0",           1007, "03-9999-2345",
              NULL, NULL, v_undef, NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1006, "admin", "2010-01-01 00:00:00.000", "S",
              "ELIAS XI CHE CHE", "7711035912", "Y", 
              "ZONA 1 BARRIO CHAJOMPEC DIAGONAL 2 ZONA 1 SAN PEDRO CARCHA ALTA VERAPAZ",    1008, "03-7645-9999",
              NULL, NULL, v_undef, NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1007, "admin", "2010-01-01 00:00:00.000", "S",
              "AMILCAR SALVADOR DE LEON DE LEON", "7049165022",   "Y", 
              "RESIDENCIALES IMPERAIL   ZONA7 3A. CA 3-59 Z. 07 RES. IMPERIAL COBAN ALTA VERAPAZ",          1002, "03-7645-9999",
              NULL, NULL, v_undef, NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1008, "admin", "2010-01-01 00:00:00.000", "S",
              "HERNAN CHUN XOL XOL", "7122025861", "Y", 
              "CASERIO PULICIBIC - CAHABON ALTA VERAPAZ",      1004, "03-9999-2345",
              NULL, NULL, v_undef, NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1009, "admin", "2010-01-01 00:00:00.000", "S",
              "JERSON EZEQUIEL SOSA SICAL SICAL", "7533050965",  "Y", 
              "28 CALLE C 7-58   CALLEJON ESPAA SAN JOSE LA COMUNIDAD ZONA 10   MIXCO GUATEMALA",      1002, "03-9999-2345",
              NULL, NULL, v_undef, NULL, NULL, NULL, NULL )
       INSERT INTO contact VALUES ( 1010, "admin", "2010-01-01 00:00:00.000", "S",
              "JOSE EFRAIN AX FERNANDEZ FERNANDEZ", "7049259238", "Y", 
              "2 CALLE BARRIO CHIBUJBU ZONA 5 SAN PEDRO CARCHA ALTA VERAPAZ",        1004, "03-7645-9999",
              NULL, NULL, v_undef, NULL, NULL, NULL, NULL )
       LET s = libutil.sequence_create("contact",2000)
    END IF

    CREATE TABLE contnote (
         contnote_num INTEGER NOT NULL,
         contnote_rec_muser VARCHAR(20) NOT NULL,
         contnote_rec_mtime DATETIME YEAR TO FRACTION(3) NOT NULL,
         contnote_rec_mstat CHAR(2) NOT NULL,
         contnote_contact INTEGER NOT NULL,
         contnote_when DATETIME YEAR TO FRACTION(3) NOT NULL,
         contnote_text VARCHAR(250),
         PRIMARY KEY(contnote_num),
         UNIQUE (contnote_contact, contnote_when),
         FOREIGN KEY (contnote_contact) REFERENCES contact (contact_num)
    )
    IF NOT ws THEN
       LET s = libutil.sequence_create("contnote",1000)
    ELSE
       INSERT INTO contnote VALUES ( 1001, "admin", "2010-01-01 00:00:00.000", "S",
              1001, "2014-01-01 11:45:00.000", "This customer is French" )
       INSERT INTO contnote VALUES ( 1002, "admin", "2010-01-01 00:00:00.000", "S",
              1001, "2014-01-02 12:43:00.000", "Send a gift for Xmass" )
       INSERT INTO contnote VALUES ( 1003, "admin", "2010-01-01 00:00:00.000", "S",
              1001, "2014-01-03 11:42:00.000", "Has many offices around the world" )
       INSERT INTO contnote VALUES ( 1004, "admin", "2010-01-01 00:00:00.000", "S",
              1002, "2014-01-01 15:15:00.000", "Next call: Friday the 12th in the afternoon" )
       LET s = libutil.sequence_create("contnote",2000)
    END IF

END FUNCTION
