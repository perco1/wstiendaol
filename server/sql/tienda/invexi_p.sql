drop table inv_exi;
create table inv_exi
  (
    bodega smallint,
    casa char(2),
    correl integer,
    ultimoc money(10,2),
    ultimopv money(10,2),
    costo money(10,2),
    margen decimal(10,2),
    preciov money(10,2),
    existi decimal(10,2),
    existd decimal(10,2),
    exi_sug decimal(10,2),
    fechau_venta date,
    fechamov date,
    tipomov char(1),
    minimo decimal(8,2),
    maximo decimal(8,2),
    estante char(3),
    rack smallint,
    tramo smallint,
    estatus char(1)
  );



create unique index i_invexi_01 on inv_exi (bodega,
    casa,correl) using btree ;
create index i_invexi_02 on inv_exi (casa,correl)
    using btree ;
create index i_invexi_03 on inv_exi (bodega,
    casa,correl,existd) using btree ;

