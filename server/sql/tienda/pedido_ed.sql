






{ TABLE "informix".pedido_ed row size = 26 number of columns = 7 index size = 24 }

create table "informix".pedido_ed 
  (
    num_pedido integer,
    casa char(2),
    correl integer,
    cantidad decimal(5,0),
    saldo decimal(5,0),
    precio_u decimal(10,2),
    estatus char(2),
    primary key (num_pedido,casa,correl)  constraint "modula".pk_pedido_ed
  );

revoke all on "informix".pedido_ed from "public" as "informix";


create index "informix".idxped_ed1 on "informix".pedido_ed (num_pedido) 
    using btree ;


alter table "informix".pedido_ed add constraint (foreign key 
    (num_pedido) references "informix".pedido_e );


